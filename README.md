# neighbourhood_map_spa
Single page application of a map. Includes functionality to highlight locations and discover data about the location via third-party.

--Specs
How will I complete this Project?
* <completed> Review our course JavaScript Design Patterns.
* <completed> Download the Knockout framework.
* <completed> Write code required to add a full-screen map to your page using the Google Maps API.
* <completed> Write code required to add map markers identifying a number of locations you are interested in within this neighborhood.
*<completed>Implement the search bar functionality to search your map markers.
*<completed>Implement a list view of the identified locations.
*Add additional functionality using third-party APIs when a map marker, search result, or list view entry is clicked (ex. Yelp reviews, Wikipedia, StreetView/Flickr images, etc). If you need a refresher on making AJAX requests to third-party servers, check out our Intro to AJAX course.

--Helpful Resources
None of these are required, but they may be helpful.

*Yelp API
*MediaWikiAPI for Wikipedia
*Google Maps Street View Service
*Google Maps