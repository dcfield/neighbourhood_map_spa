var markers = [
	{
		title: 'virginia',
		position: {lat: 53.8360571, lng: -7.0851975}
	},
	{
		title: 'marker_2',
		position: {lat: 53.821621, lng: -7.051295}
	},
	{
		title: 'marker_3',
		position: {lat: 53.821656, lng: -7.055295}
	}
];

var ViewModel = function(){
	var self = this;

	this.markersList = ko.observableArray([]);
	
	// Functionality to query our markers
	this.query = ko.observable('');
	this.search = function(){
		$.each(self.markersList(), function(i, item){
			item().setMap(null);
			if(item().title.toLowerCase().indexOf(self.query().toLowerCase()) >= 0){
				item().setMap(self.map);
			}
		})
	}
	
	// Loop through markers array and create an observableArray
	markers.forEach(function(markerItem){
		this.data = {
			position: markerItem.position,
			title: markerItem.title
		}
		this.marker = ko.observable(new google.maps.Marker(this.data));
		self.markersList.push(this.marker);
	});
	
	// Set query variable
	self.query = ko.observable('');
	
	self.filteredMarkers = ko.computed(function(){
		var filter = self.query().toLowerCase();
		//dconsole.log('hello');
		if(!filter){
			return self.markersList();
		}else{
			return ko.utils.arrayFilter(self.markersList(), function(item){
				var doesMatch = item().title.toLowerCase().indexOf(filter) >= 0;
				
				self.setMarker(this);
				
				self.setMarkerVisibility(doesMatch);
				
				return doesMatch;
			});
		}
	});
	
	self.setMarkerVisibility = function(boolean){
		self.currentMarker.isVisible = boolean;
	}
	
	// Set current marker
	self.currentMarker = this.filteredMarkers()[0];
	
	self.setMarker = function(marker){
		self.currentMarker = marker;
	}
	
	self.map = new google.maps.Map(document.getElementById('map'), {
      zoom: 12,
      center: self.currentMarker().position
    });
	
	self.filteredMarkers().forEach(function(markerItem){
		markerItem().setMap(self.map);
	});

	self.zomato = new ZomatoModel();
	self.zomato.queryZomato();
}

var initMap = function(){
	ko.applyBindings(new ViewModel());
}

var ZomatoModel = function(){

	this.queryZomato = function(){
		$.ajax({
		    url: 'https://developers.zomato.com/api/v2.1/categories',
		    headers: {
		        'user-key':'7ac1f4a09db0acc9fe79de9a650d4c1c'
		    },
		    method: 'POST',
		    dataType: 'json',
		    //data: YourData,
		    success: function(data){
		      console.log(data);
		    }
		  });
	}
}

